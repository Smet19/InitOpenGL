#include "GameController.h"
#include "WindowController.h"
//#include "ToolWindow.h"
#include "Fonts.h"

GameController::GameController()
{
	m_shaderColor = { };
	m_shaderDiffuse = { };
	m_camera = { };
	m_meshBoxes.clear();
}

GameController::~GameController()
{
}

void GameController::Initialize()
{
	GLFWwindow* window = WindowController::GetInstance().GetWindow();
	M_ASSERT(glewInit() == GLEW_OK, "Failed to initialize GLEW");
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glClearColor(0.1f, 0.1f, 0.1, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	srand(time(0));

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	m_camera = Camera(WindowController::GetInstance().GetResolution());
}

void GameController::RunGame()
{
	m_shaderColor = Shader();
	m_shaderColor.LoadShaders("Color.vert", "Color.frag");
	m_shaderDiffuse = Shader();
	m_shaderDiffuse.LoadShaders("Diffuse.vert", "Diffuse.frag");
	m_shaderSkybox = Shader();
	m_shaderSkybox.LoadShaders("Skybox.vert", "Skybox.frag");
	m_shaderFont = Shader();
	m_shaderFont.LoadShaders("Font.vert", "Font.frag");

	Mesh m = Mesh();
	m.Create(&m_shaderColor, "../Assets/Models/teapot.obj");
	m.SetPosition({ 0.0f, 0.8f, 1.0f });
	m.SetColor({ 1.0f, 1.0f, 1.0f });
	m.SetScale({ 0.01f, 0.01f, 0.01f});
	Mesh::Lights.push_back(m);

	//Mesh fighter = Mesh();
	//fighter.Create(&m_shaderDiffuse, "../Assets/Models/Fighter.obj");
	//fighter.SetCameraPosition(m_camera.GetPosition());
	//fighter.SetScale({ 0.0015f, 0.0015f, 0.0015f });
	//fighter.SetPosition({ 0.0f, 0.0f, 0.0f });
	//m_meshBoxes.push_back(fighter);

	Mesh cube = Mesh();
	cube.Create(&m_shaderDiffuse, "../Assets/Models/Cube.obj", 1000);
	cube.SetCameraPosition(m_camera.GetPosition());
	cube.SetScale({ 0.1f, 0.1f, 0.1f });
	cube.SetPosition({ 0.25f, 0.25f, 0.25f });
	m_meshBoxes.push_back(cube);

	//Mesh wall = Mesh();
	//wall.Create(&m_shaderDiffuse, "../Assets/Models/Wall.obj");
	//wall.SetCameraPosition(m_camera.GetPosition());
	//wall.SetScale({ 0.05f, 0.05f, 0.05f });
	//wall.SetPosition({ 0.0f, 0.0f, 0.0f });
	//m_meshBoxes.push_back(wall);

	//Skybox skybox = Skybox();
	//skybox.Create(&m_shaderSkybox, "../Assets/Models/Skybox.obj",
	//	{	"../Assets/Textures/Skybox/right.jpg",
	//		"../Assets/Textures/Skybox/left.jpg",
	//		"../Assets/Textures/Skybox/top.jpg",
	//		"../Assets/Textures/Skybox/bottom.jpg",
	//		"../Assets/Textures/Skybox/front.jpg",
	//		"../Assets/Textures/Skybox/back.jpg" });


	//Mesh plane = Mesh();
	//plane.Create(&m_shaderDiffuse, "../Assets/Models/Plane.obj");
	//plane.SetCameraPosition(m_camera.GetPosition());
	//plane.SetScale({ 0.3f, 0.3f, 0.3f });
	//plane.SetPosition({ 0.0f, 0.0f, 0.0f });
	//m_meshBoxes.push_back(plane);

	//Mesh window = Mesh();
	//window.Create(&m_shaderDiffuse, "../Assets/Models/Window.obj");
	//window.SetCameraPosition(m_camera.GetPosition());
	//window.SetScale({ 0.1f, 0.1f, 0.1f });
	//window.SetPosition({ 0.0f, 0.0f, 0.0f });
	//m_meshBoxes.push_back(window);

	Fonts f = Fonts();
	f.Create(&m_shaderFont, "arial.ttf", 100);

	double lastTime = glfwGetTime();
	int fps = 0;
	string fpsStr = "0";

	GLFWwindow* win = WindowController::GetInstance().GetWindow();
	do
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		double currentTime = glfwGetTime();
		fps++;
		if (currentTime - lastTime >= 1.0)
		{
			fpsStr = "FPS: " + to_string(fps);
			fps = 0;
			lastTime += 1.0f;
		}
		f.RenderText(fpsStr, 100, 100, 0.5f, { 1.0f, 1.0f, 1.0f });

		//m_camera.Rotate();
		//glm::mat4 view = glm::mat4(glm::mat3(m_camera.GetView()));
		//skybox.Render(m_camera.GetProjection() * view);

		for (auto& box : m_meshBoxes)
		{
			box.Render(m_camera.GetProjection() * m_camera.GetView());
		}

		for (auto& light : Mesh::Lights)
		{
			light.Render(m_camera.GetProjection() * m_camera.GetView());
		}

		f.RenderText("Testing text", 10, 500, 0.5f, { 1.0f, 1.0f, 0.0f });

		glfwSwapBuffers(win);
		glfwPollEvents();

	} while (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS
		&& glfwWindowShouldClose(win) == 0);

	for (auto& box : m_meshBoxes)
	{
		box.Cleanup();
	}

	for (auto& light : Mesh::Lights)
	{
		light.Cleanup();
	}
	//skybox.Cleanup();
	m_shaderDiffuse.Cleanup();
	m_shaderColor.Cleanup();
	m_shaderSkybox.Cleanup();
}