#version 330

in vec3 vertices;

uniform mat4 PV;

out vec3 retTexCoords;

void main()
{
	retTexCoords = vertices;
	gl_Position = PV * vec4(vertices, 1.0);
}