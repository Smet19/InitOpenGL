#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "StandardIncludes.h"

class Texture
{
public:
	// Constructors / Destructors
	Texture();
	virtual ~Texture() { };

	// Accessors
	GLuint GetTexture() { return m_texture; }

	// Methods
	void LoadTexture(string _fileName);
	void LoadCubemap(vector<std::string> _faces);
	void Cleanup();

private:
	// Members
	int m_width;
	int m_height;
	int m_channels;
	GLuint m_texture;

	//Methods
	bool EndsWith(const std::string& _str, const std::string& _suffix);
};

#endif // !_TEXTURE_H_

